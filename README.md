# Squirrel Windows Module

An Autofac module that updates the application using the Squirrel.Windows package.

## Usage

Register the module in the Container.

```C#
var builder = new ContainerBuilder();
builder.RegisterModule(new SquirrelWindowsModule.SquirrelWindowsModule() {
	Assembly = Assembly.GetEntryAssembly() ?? Assembly.GetExecutingAssembly(),
	AssemblyLocation = this.Assembly.Location,
	CreateAppRootShortcut = false,
	CreateDesktopShortcut = true,
	CreateStartMenuShortcut = true,
});
```

Set the update path or url via this static variable:

```
SquirrelWindowsModule.SquirrelWindowsModuleConfiguration.UpdateUrl = @"Path\To\Update\Folder";
```

If implementing the `SquirrelWindowsModule.IServices.IRegisterUpdateAppEventsService`, add the following line in the `AssemblyInfo.cs` file:

```C#
[assembly: AssemblyMetadata("SquirrelAwareVersion", "1")]
```

Implement the services `SquirrelWindowsModule.IServices.IRegisterUpdateAppEventsService` and `SquirrelWindowsModule.IServices.IUpdateAppService`.

```C#
using SquirrelWindowsModule.IServices;
// other imports
		
namespace SampleNamespace
{
	public class SampleClass
	{
		private readonly IRegisterUpdateAppEventsService _RegisterUpdateAppEventsService;
		private readonly IUpdateAppService _UpdateAppService;
				
		public SampleClass(IRegisterUpdateAppEventsService registerUpdateAppEventsService, IUpdateAppService updateAppService) {
			this._RegisterUpdateAppEventsService= registerUpdateAppEventsService;
			this._UpdateAppService = updateAppService;
			// other implementations
		}
				
		public void SampleMethod() {
			this._RegisterUpdateAppEventsService.Register(null, null, m => Debug.WriteLine(m));
			this._UpdateAppService.UpdateAsync(true, m => Debug.WriteLine(m));
		}
	}
}
```
