﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SquirrelWindowsModule.IServices;

namespace SquirrelWindowsModuleTests
{
    [TestClass]
    public class ResolvableTest : TestBase
    {
        private IRegisterUpdateAppEventsService _RegisterUpdateAppEventsService;
        private IUpdateAppService _UpdateAppService;

        [TestInitialize]
        public void Setup()
        {
            this._RegisterUpdateAppEventsService = this.Resolve<IRegisterUpdateAppEventsService>();
            this._UpdateAppService = this.Resolve<IUpdateAppService>();
        }

        [TestCleanup]
        public void TearDown()
        {
            this.DisposeContainer();
        }

        [TestMethod]
        public void RegisterUpdateAppEventsServiceResolvable()
        {
            Assert.IsNotNull(this._RegisterUpdateAppEventsService != null);
        }

        [TestMethod]
        public void UpdateAppServiceResolvable()
        {
            Assert.IsNotNull(this._UpdateAppService != null);
        }
    }
}
