using Autofac;

namespace SquirrelWindowsModuleTests
{
    public abstract class TestBase
    {
        private IContainer _Container;

        public TestBase()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<SquirrelWindowsModule.SquirrelWindowsModule>();

            this._Container = builder.Build();
        }

        protected T Resolve<T>()
        {
            return this._Container.Resolve<T>();
        }

        protected void DisposeContainer()
        {
            this._Container.Dispose();
        }
    }
}