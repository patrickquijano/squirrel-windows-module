﻿using Squirrel;

namespace SquirrelWindowsModule.IFactories
{
    internal interface IUpdateManagerFactory
    {
        UpdateManager Create();
    }
}