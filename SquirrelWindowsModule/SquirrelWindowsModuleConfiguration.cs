﻿namespace SquirrelWindowsModule
{
    public static class SquirrelWindowsModuleConfiguration
    {
        /// <summary>
        /// The path or url where the update is located.
        /// </summary>
        public static string UpdateUrl { get; set; }
    }
}
