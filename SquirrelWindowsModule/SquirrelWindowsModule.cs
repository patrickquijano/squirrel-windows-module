﻿using Autofac;
using SquirrelWindowsModule.Factories;
using SquirrelWindowsModule.IFactories;
using SquirrelWindowsModule.IServices;
using SquirrelWindowsModule.Services;
using System.Reflection;

namespace SquirrelWindowsModule
{
    public class SquirrelWindowsModule : Autofac.Module
    {
        /// <summary>
        /// The Assembly.
        /// </summary>
        public Assembly Assembly { get; set; }
        /// <summary>
        /// The Assembly's location.
        /// </summary>
        public string AssemblyLocation { get; set; }
        /// <summary>
        /// Indicator if the updater will create a app root shortcut.
        /// </summary>
        public bool CreateAppRootShortcut { get; set; }
        /// <summary>
        /// Indicator if the updater will create a desktop shortcut.
        /// </summary>
        public bool CreateDesktopShortcut { get; set; }
        /// <summary>
        /// Indicator if the updater will create a start menu shortcut.
        /// </summary>
        public bool CreateStartMenuShortcut { get; set; }

        public SquirrelWindowsModule()
        {
            this.Assembly = Assembly.GetEntryAssembly() ?? Assembly.GetExecutingAssembly();
            this.AssemblyLocation = this.Assembly.Location;
            this.CreateAppRootShortcut = false;
            this.CreateDesktopShortcut = true;
            this.CreateStartMenuShortcut = true;
        }

        protected override void Load(ContainerBuilder builder)
        {
            // Register factories
            builder.RegisterType<UpdateManagerFactory>().As<IUpdateManagerFactory>();

            // Register services
            builder.RegisterType<RegisterUpdateAppEventsService>().As<IRegisterUpdateAppEventsService>()
                   .WithParameter("assemblyLocation", this.AssemblyLocation)
                   .WithParameter("createAppRootShortcut", this.CreateAppRootShortcut)
                   .WithParameter("createDesktopShortcut", this.CreateDesktopShortcut)
                   .WithParameter("createStartMenuShortcut", this.CreateStartMenuShortcut);
            builder.RegisterType<UpdateAppService>().As<IUpdateAppService>();
        }
    }
}
