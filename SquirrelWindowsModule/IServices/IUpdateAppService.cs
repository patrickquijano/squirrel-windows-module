﻿using System;
using System.Threading.Tasks;

namespace SquirrelWindowsModule.IServices
{
    public interface IUpdateAppService
    {
        /// <summary>
        /// Checks for update and updates the app in the background without restarting the app.
        /// </summary>
        /// <param name="restart">Indicator if app needs to restart after updating.</param>
        Task UpdateAsync(bool restart);
        /// <summary>
        /// Checks for update and updates the app in the background. The <paramref name="restart" /> indicates if the app will restart after updating.
        /// </summary>
        /// <param name="restart">Indicator if app needs to restart after updating.</param>
        /// <param name="messageCallback">The message callback.</param>
        Task UpdateAsync(bool restart, Action<string> messageCallback);
    }
}