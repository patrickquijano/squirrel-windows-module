﻿using System;

namespace SquirrelWindowsModule.IServices
{
    public interface IRegisterUpdateAppEventsService
    {
        /// <summary>
        /// Registers the Squirrel.SquirrelAwareApp.HandleEvents.
        /// </summary>
        /// <param name="appInstallOrUpdateCallback">The callback on application install or update.</param>
        /// <param name="appUninstallCallback">The callback on application uninstall.</param>
        void Register(Action appInstallOrUpdateCallback, Action appUninstallCallback);
        /// <summary>
        /// Registers the Squirrel.SquirrelAwareApp.HandleEvents.
        /// </summary>
        /// <param name="appInstallOrUpdateCallback">The callback on application install or update.</param>
        /// <param name="appUninstallCallback">The callback on application uninstall.</param>
        /// <param name="messageCallback">The message callback.</param>
        void Register(Action appInstallOrUpdateCallback, Action appUninstallCallback, Action<string> messageCallback);
    }
}