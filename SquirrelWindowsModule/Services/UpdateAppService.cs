﻿using NuGet;
using Squirrel;
using SquirrelWindowsModule.IFactories;
using SquirrelWindowsModule.IServices;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace SquirrelWindowsModule.Services
{
    internal class UpdateAppService : IUpdateAppService
    {
        private readonly IUpdateManagerFactory _UpdateManagerFactory;

        public UpdateAppService(IUpdateManagerFactory updateManagerFactory)
        {
            this._UpdateManagerFactory = updateManagerFactory;
        }

        /// <summary>
        /// Checks for update and updates the app in the background without restarting the app.
        /// </summary>
        /// <param name="restart">Indicator if app needs to restart after updating.</param>
        public Task UpdateAsync(bool restart)
        {
            return this.UpdateAsync(false, null);
        }

        /// <summary>
        /// Checks for update and updates the app in the background. The <paramref name="restart" /> indicates if the app will restart after updating.
        /// </summary>
        /// <param name="restart">The indicator if the app will restart after updating.</param>
        /// <param name="messageCallback">The message callback.</param>
        public async Task UpdateAsync(bool restart, Action<string> messageCallback)
        {
            if (Debugger.IsAttached)
            {
                messageCallback?.Invoke("Cannot perform updates while on development.");

                return;
            }
            messageCallback?.Invoke("Checking updates...");
            using (var updateManager = this._UpdateManagerFactory.Create())
            {
                var updateInfo = await updateManager.CheckForUpdate();
                var currentVersion = updateInfo.CurrentlyInstalledVersion == null ? SemanticVersion.Parse("1.0.0") : updateInfo.CurrentlyInstalledVersion.Version;
                var futureVersion = updateInfo.FutureReleaseEntry.Version;
                messageCallback?.Invoke($"Current version is {currentVersion}.");
                if (currentVersion == futureVersion)
                {
                    messageCallback?.Invoke($"No updates available.");

                    return;
                }
                messageCallback?.Invoke($"Updating to version is {futureVersion}.");
                var releaseEntry = await updateManager.UpdateApp();
                if (releaseEntry.Version != futureVersion)
                {
                    messageCallback?.Invoke("The application has not been updated.");

                    return;
                }
                messageCallback?.Invoke($"The application has been updated to {releaseEntry.Version}.");
                if (!restart)
                {
                    return;
                }
                messageCallback?.Invoke("The application is now restarting.");
                UpdateManager.RestartApp();
            }
        }
    }
}
