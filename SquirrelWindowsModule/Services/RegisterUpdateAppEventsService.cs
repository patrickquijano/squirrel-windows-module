﻿using Squirrel;
using SquirrelWindowsModule.IFactories;
using SquirrelWindowsModule.IServices;
using System;
using System.Diagnostics;
using System.IO;

namespace SquirrelWindowsModule.Services
{
    internal class RegisterUpdateAppEventsService : IRegisterUpdateAppEventsService
    {
        private readonly string _AssemblyLocation;
        private readonly bool _CreateAppRootShortcut;
        private readonly bool _CreateDesktopShortcut;
        private readonly bool _CreateStartMenuShortcut;
        private readonly IUpdateManagerFactory _UpdateManagerFactory;

        public RegisterUpdateAppEventsService(string assemblyLocation,
                                              bool createAppRootShortcut,
                                              bool createDesktopShortcut,
                                              bool createStartMenuShortcut,
                                              IUpdateManagerFactory updateManagerFactory)
        {
            this._AssemblyLocation = assemblyLocation;
            this._CreateAppRootShortcut = createAppRootShortcut;
            this._CreateDesktopShortcut = createDesktopShortcut;
            this._CreateStartMenuShortcut = createStartMenuShortcut;
            this._UpdateManagerFactory = updateManagerFactory;
        }

        /// <summary>
        /// Registers the Squirrel.SquirrelAwareApp.HandleEvents.
        /// </summary>
        /// <param name="appInstallOrUpdateCallback">The callback on application install or update.</param>
        /// <param name="appUninstallCallback">The callback on application uninstall.</param>
        public void Register(Action appInstallOrUpdateCallback, Action appUninstallCallback)
        {
            this.Register(appInstallOrUpdateCallback, appUninstallCallback, null);
        }

        /// <summary>
        /// Registers the Squirrel.SquirrelAwareApp.HandleEvents.
        /// </summary>
        /// <param name="appInstallOrUpdateCallback">The callback on application install or update.</param>
        /// <param name="appUninstallCallback">The callback on application uninstall.</param>
        /// <param name="messageCallback">The message callback.</param>
        public void Register(Action appInstallOrUpdateCallback, Action appUninstallCallback, Action<string> messageCallback)
        {
            if (Debugger.IsAttached)
            {
                messageCallback?.Invoke("Cannot perform update app event registration while on development.");

                return;
            }
            messageCallback?.Invoke("Registering update app event updates.");
            using (var updateManager = this._UpdateManagerFactory.Create())
            {
                var shortcutLocation = (ShortcutLocation?)null;
                if (this._CreateAppRootShortcut)
                {
                    shortcutLocation = shortcutLocation.HasValue ? shortcutLocation | ShortcutLocation.AppRoot : ShortcutLocation.AppRoot;
                }
                if (this._CreateDesktopShortcut)
                {
                    shortcutLocation = shortcutLocation.HasValue ? shortcutLocation | ShortcutLocation.Desktop : ShortcutLocation.Desktop;
                }
                if (this._CreateStartMenuShortcut)
                {
                    shortcutLocation = shortcutLocation.HasValue ? shortcutLocation | ShortcutLocation.StartMenu : ShortcutLocation.StartMenu;
                }
                SquirrelAwareApp.HandleEvents(onInitialInstall: this.OnAppInstallOrUpdate(updateManager, shortcutLocation, appInstallOrUpdateCallback),
                                              onAppUpdate: this.OnAppInstallOrUpdate(updateManager, shortcutLocation, appInstallOrUpdateCallback),
                                              onAppUninstall: this.OnAppUninstall(updateManager, appUninstallCallback));
            }
            messageCallback?.Invoke("Successfully registered update app event updates.");
        }

        private Action<Version> OnAppInstallOrUpdate(UpdateManager updateManager, ShortcutLocation? shortcutLocation, Action appInstallOrUpdateCallback)
        {
            return _ =>
            {
                updateManager.RemoveShortcutForThisExe();
                if (shortcutLocation.HasValue)
                {
                    updateManager.CreateShortcutsForExecutable(Path.GetFileName(this._AssemblyLocation), shortcutLocation.Value, false);
                }
                appInstallOrUpdateCallback?.Invoke();
            };
        }

        private Action<Version> OnAppUninstall(UpdateManager updateManager, Action appUninstallCallback)
        {
            return _ =>
            {
                updateManager.RemoveShortcutForThisExe();
                appUninstallCallback?.Invoke();
            };
        }
    }
}
