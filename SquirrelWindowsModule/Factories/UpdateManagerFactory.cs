﻿using Squirrel;
using SquirrelWindowsModule.IFactories;

namespace SquirrelWindowsModule.Factories
{
    internal class UpdateManagerFactory : IUpdateManagerFactory
    {
        /// <summary>
        /// Creates an instance of Squirrel.UpdateManager.
        /// </summary>
        /// <returns>The instance of Squirrel.UpdateManager.</returns>
        public UpdateManager Create()
        {
            return new UpdateManager(SquirrelWindowsModuleConfiguration.UpdateUrl);
        }
    }
}
